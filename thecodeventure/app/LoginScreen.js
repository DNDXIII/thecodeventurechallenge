import React, { Component } from 'react';
import { View, Linking, Button, Image, StyleSheet } from 'react-native';
const config = require('../config.js');
const logo = require('../images/GitLogo.png')

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        Linking.addEventListener('url', this.handleOpenURL);
    }
    componentWillUnmount() {
        Linking.removeEventListener('url', this.handleOpenURL);
    }

    onPressLogin() {
        Linking.openURL([
            'https://github.com/login/oauth/authorize',
            '?client_id=' + config.app_key,
        ].join(''))
            .catch(err => console.error('An error occurred', err));
    }

    handleOpenURL = (event) => {
        var code = event.url.toString().match(/code=([^&]+)/);
        console.log(code[1]);
        if (code[1]) {
            this.props.setLogin(true);
        }
    }


    render() {
        return (
            <View style={styles.container} >
                <Image
                    source={logo}
                    style={styles.image}
                />

                < Button
                    onPress={this.onPressLogin}
                    title="Login with Github"
                />

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        marginTop:'25%',
        justifyContent:'center', 
        alignItems:'center'
    },
    image: {
        height: 200,
        width: 200,
        marginBottom: 20,
    },


});





