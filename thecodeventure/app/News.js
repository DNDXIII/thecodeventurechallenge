import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Linking } from 'react-native';

export default class News extends Component {
    constructor(props) {
        super(props);
    }

    onPressLink(url) {
        Linking.openURL(url);
    }

    getDate(date) {
        //date comes in unix format
        var timeDiff = Math.abs(new Date() / 1000 - new Date(date));
        return Math.floor(timeDiff / 3600);
    }

    render() {
        return (
            <View style={styles.container} >
                <Text style={styles.title} onPress={(url) => this.onPressLink(this.props.url)} >{this.props.title} </Text>
                <Text>
                    {this.props.score} points | <Text onPress={(url) => this.onPressLink()}>
                        {this.props.comments} comments | </Text>
                    {this.getDate(this.props.time)} hours ago
                    by {this.props.by}
                </Text>

            </View >
        );
    }

}

const styles = StyleSheet.create({
    container: {
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderColor: "#e8e8e8",
        margin: 3,
        paddingLeft: 10,
        paddingRight: 10,

    },
    title: {
        fontWeight: "400",
        color: "black",
        fontSize: 16,
    }
});