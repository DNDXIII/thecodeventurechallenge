import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Text, View, Button, Linking, StyleSheet } from 'react-native';
import News from './News'

export default class NewsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            dataSource: [],
        }
    }

    componentDidMount() {
        fetch('https://hacker-news.firebaseio.com/v0/topstories.json')
            .catch(error => { console.error(error) })
            .then((response) => response.json())
            .then((responseJson) => {
                let ids = [];
                for (i = 0; i < 30; i++)
                    ids.push(responseJson[i]);
                return ids;
            }).then((ids) => {
                Promise.all(ids.map((id) => fetch('https://hacker-news.firebaseio.com/v0/item/' + id + '.json')))
                    .then(responses => Promise.all(responses.map(res => res.json())))
                    .then((responsesJson) => {
                        this.setState({
                            dataSource: responsesJson,
                            isLoading: false,

                        })
                    })
            })
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View >
                    <ActivityIndicator
                        style={styles.activityIndicator}
                        size='large' />
                </View>
            );
        }

        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor={(item, index) => item.id}
                    data={this.state.dataSource}
                    renderItem={({ item }) => <News
                        title={item.title}
                        score={item.score}
                        comments={item.descendants}
                        time={item.time}
                        by={item.by}
                        url={item.url} />
                    }
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    activityIndicator: {
        marginTop: '60%'
    },

    container: {
        
    }
});