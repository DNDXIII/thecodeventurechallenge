import React, { Component } from 'react';
import { View, Text } from 'react-native';
import NewsList from './NewsList';
import LoginScreen from './LoginScreen';
import { Header } from 'react-native-elements';


export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: false,
        }
    }

    setLogin = (val) => {
        this.setState({
            loggedIn: val,
        })
    }



    render() {
        return (
            this.state.loggedIn ? (
                <View >
                    <Header
                        leftComponent={{ icon: 'menu', color: '#fff' }}
                        centerComponent={{ text: 'Hacker News', style: { color: '#fff', fontSize: 24 } }}
                        rightComponent={<Text style={{ color: '#fff', fontSize: 18 }} onPress={() => this.setLogin(false)}>Logout</Text>}
                    />
                    <NewsList setLogin={this.setLogin} />
                </View>
            )
                : (
                    <View >
                        <Header
                            leftComponent={{ icon: 'menu', color: '#fff' }}
                            centerComponent={{ text: 'Hacker News', style: { color: '#fff', fontSize: 24 } }}
                        />
                        <LoginScreen setLogin={this.setLogin} />
                    </View>
                )
        );
    }
}


