import React from 'react';
import { View } from 'react-native';
import Home from './app/Home';


export default class App extends React.Component {
  render() {
    return (
      <View >
        <Home />
      </View>
    );
  }
}

